﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenueController : MonoBehaviour
{
    public Button startButton;
    public bool FinshedIntro = false;

    private bool isLoadingScene = false;
    private bool working = false;

    // Start is called before the first frame update
    void Start()
    {
        //add listern to button, tell button what function to run
        startButton.onClick.AddListener(ClickedButton);
        startButton.gameObject.SetActive(false);

        Camera.main.GetComponent<Animator>().Play("Intro",0,0);

    }

    // Update is called once per frame
    void Update()
    {
        if(isLoadingScene && !working)
        {
            StartCoroutine(StartLoadingScreen());
            working = true;
        }

        if(FinshedIntro )
        {
            startButton.gameObject.SetActive(true);
            Camera.main.GetComponent<Animator>().enabled = false;
        }

    }

    //update the varibale to start the loading sequence
    void ClickedButton()
    {
        if( FinshedIntro )
        {

            isLoadingScene = true;
        }

    }

    //starts loading the loadin screen scene
    IEnumerator StartLoadingScreen()
    {

        // This line waits for 3 seconds before executing the next line in the coroutine.
        // This line is only necessary for this demo. The scenes are so simple that they load too fast to read the "Loading..." text.
        //yield return new WaitForSeconds();

        // Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
        AsyncOperation async = SceneManager.LoadSceneAsync("LoadingScene");//Application.LoadLevelAsync(scene);

        // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
        while (!async.isDone)
        {
            yield return null;
        }

    }

}
