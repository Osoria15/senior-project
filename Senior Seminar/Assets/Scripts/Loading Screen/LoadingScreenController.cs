﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreenController : MonoBehaviour
{
    public Slider loadingBar;

    public Text text;

    private float _progress = 0f;

    private bool _isClicked = false;

    //on start, start a coroutine to load the level
    private void Start()
    {
        StartCoroutine(LoadAsync("Scene 3D"));
    }

    IEnumerator LoadAsync(string sceneName)
    {
        //start async opreation to load level
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

        //prevent scene from loading as soon as its done
        operation.allowSceneActivation = false;

        //while the scene hasnt finished loading
        while (_progress < 1f)
        {
            //clamp progress to a value between 0 and 1
            _progress = Mathf.Clamp01(operation.progress / 0.9f);

            //update the loading bar process
            loadingBar.value = _progress;

            //set the text value
            text.text = "Loading... " + (int)(_progress * 100f) + "%";

            yield return null;
        }

        text.text = "Click anywhere to start.";

        //while the user hasnt clicked to start the game, do nothing
        while (!_isClicked)
        {
            yield return null;
        }

        //if the user clicked then start scene
        operation.allowSceneActivation = true;
    }

    private void Update()
    {
        //check for click if progress is finished
        if (Input.GetMouseButtonUp(0) && _progress == 1f)
        {
            _isClicked = true;
        }
    }
}