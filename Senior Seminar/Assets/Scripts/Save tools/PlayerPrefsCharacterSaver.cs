﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsCharacterSaver : MonoBehaviour
{
    public SettingsData settingsData;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
            SavePrefrences(settingsData);

        if (Input.GetKeyDown(KeyCode.L))
            settingsData = LoadPrefrences();
    }

    static void SavePrefrences(SettingsData data)
    {
        PlayerPrefs.SetInt("InvertVerticalLook", data.invertVerticalLook);
        PlayerPrefs.Save();
    }

    static SettingsData LoadPrefrences()
    {
        SettingsData loadedPrefrences = new SettingsData();
        loadedPrefrences.invertVerticalLook = PlayerPrefs.GetInt("InvertVerticalLook");


        return loadedPrefrences;
    }
}
